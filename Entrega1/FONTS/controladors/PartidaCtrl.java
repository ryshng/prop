package domini.controladors;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static java.lang.Integer.parseInt;

public class PartidaCtrl {

    private static Jugador jugador1, jugador2;
    private static int prof1, prof2;
    private static boolean pod1, pod2;
    private static Partida partida;
    private static int torn = 0;

    public static void main(String[] args) throws IOException, CloneNotSupportedException {
        iniciarPartida();
    }

    public static void iniciarPartida() throws IOException, CloneNotSupportedException {
        IO.print( "Hola, sóc el controlador de la classe Partida" );
        IO.print( "Selecciona el mode de joc:" );
        IO.print( "1 -> Persona vs Persona" );
        IO.print( "2 -> Persona vs Màquina" );
        IO.print( "3 -> Màquina vs Màquina" );
        IO.print( "4 -> Sortir" );
        String input = IO.read();
        Tauler tauler = new Tauler(1, true, true, true);
        switch(input) {
            case "1":
                jugador1 = new Perfil("SegFault");
                jugador2 = new Perfil("NullPointer");
                partida = new Partida(1, jugador1, jugador2, tauler);
                play();
                break;
            case "2":
                jugador1 = new Perfil("SegFault");
                Maquina maquina2 = new Maquina("NullPointer");
                partida = new Partida(1, jugador1, maquina2, tauler);
                IO.print("Entra la profunditat de la màquina");
                prof2 = parseInt(IO.read());
                IO.print("Entra si la màquina farà podes (s) o no (n)");
                pod2 = IO.read().equals("s");
                play();
                break;
            case "3":
                Maquina maquina1 = new Maquina("SegFault");
                maquina2 = new Maquina("NullPointer");
                partida = new Partida(1, maquina1, maquina2, tauler);
                IO.print("NEGRES:");
                IO.print("Entra la profunditat de la màquina");
                prof1 = parseInt(IO.read());
                IO.print("Entra si la màquina farà podes (s) o no (n)");
                pod2 = IO.read().equals("s");
                IO.print("BLANQUES:");
                IO.print("Entra la profunditat de la màquina");
                prof2 = parseInt(IO.read());
                IO.print("Entra si la màquina farà podes (s) o no (n)");
                pod2 = IO.read().equals("s");

                play();
                break;
            case "4":
                sortir();
                break;
            default:
                IO.print( "Si us plau, entra un valor de l'1 al 4" );
                iniciarPartida();
        }
    }

    private static void play() throws IOException, CloneNotSupportedException {
        IO.print("Comença la partida");
        while (!partida.getAcabada()) {
            printInfo();
            posarFitxa();
            partida.canviaTorn();
            ++torn;
            if (partida.getTauler().isacabat()) partida.getAcabada(true);
        }
        IO.print("FINAL DE LA PARTIDA!");
        IO.printFitxesPartida(partida);
        if (partida.getTauler().getFitxesB() ==  partida.getTauler().getFitxesN()) IO.print("Empat");
        else if (partida.getTauler().getFitxesB() > partida.getTauler().getFitxesN()) IO.print("Guanyen les Blanques");
        else IO.print("Guanyen les Negres");
        IO.print("----------------------------------------------------------------");
        iniciarPartida();
    }

    private static void posarFitxa() throws IOException, CloneNotSupportedException {
        if ((partida.getTorn() && jugador1 instanceof Perfil) || (!partida.getTorn() && jugador2 instanceof Perfil)) posarFitxaPersona();
        else posarFitxaMaquina(partida.getTorn());
    }

    private static void posarFitxaPersona() throws IOException {
        IO.print( "Col·loca una fitxa introduïnt la casella, p.ex: a4, f8, e6" );
        String input = IO.read();
        if (input.length() != 2) {
            IO.print( "Introdueix bé la casella! (max length = 2)" );
            posarFitxaPersona();
        }
        int column = input.charAt(0) - 97;
        int row = parseInt(input.substring(1));
        row = 8 - row;
        IO.print(row);
        IO.print(column);
        if (column < 0 || column > 8 || row < 0 || row > 8) {
            IO.print( "Introdueix bé la casella! (out of range)" );
            posarFitxaPersona();
        }
        Color col;
        if (partida.getTorn()) col = Color.Negre;
        else col = Color.Blanc;
        if (!partida.getTauler().posarFitxa(new Pair(row, column), col)) {
            IO.print( "Posició no vàlida!" );
            posarFitxaPersona();
        }
    }

    private static void posarFitxaMaquina(boolean color) throws CloneNotSupportedException {
        Color torn;
        Pair p;
        if (color) {
            torn = Color.Negre;
            p = Maquina.decidirJugada(partida.getTauler(), torn, prof1, pod1, false);
        } else {
            torn = Color.Blanc;
            p = Maquina.decidirJugada(partida.getTauler(), torn, prof2, pod2, true);
        }
        IO.printNoBreak("\nJuga la màquina -> ");
        if (p != null) {
            IO.print(p);
            IO.print("\n");
            partida.getTauler().posarFitxa(p, torn);
        } else {
            IO.print("Jugador " + torn + " passa el torn\n");
        }
    }

    private static void printInfo() {
        IO.printNoBreak("----------- TORN: ");
        IO.printNoBreak(torn);
        IO.print(" -----------\n");
        IO.print("INFO:");
        IO.printTorn(partida.getTorn());
        IO.printFitxesPartida(partida);
        IO.print("\nTAULER:");
        IO.print(partida.getTauler().getTauler());
    }

    private static void sortir() {
        IO.print( "A reveure!" );
    }
}
