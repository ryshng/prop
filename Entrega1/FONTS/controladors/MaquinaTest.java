package domini.controladors;

import domini.classes.Maquina;
import domini.classes.Tauler;
import domini.shared.Color;
import domini.shared.Pair;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MaquinaTest {

    public static void main(String[] args) throws IOException, CloneNotSupportedException {
        IO.print("Selecciona el mètode a testejar: ");
        IO.print("1 -> generaMoviments");
        IO.print("2 -> decidirJugada");
        String input = IO.read();
        switch(input) {
            case "1":
                testGeneraMoviments();
                break;
            case "2":
                testDecidirJugada();
                break;
            default:
                IO.print("Introdueix un nombre de l'1 al 2");
        }
        main(new String[]{""});
    }

    private static void testGeneraMoviments() throws IOException {
        System.out.println("\n********************** TEST DEL MÈTODE generaMoviments **********************\n");
        String path = "jocsProves/Maquina/generaMoviments";
        ArrayList<String> fileNames = IO.llegeixFitxers(new File(path));
        Tauler tauler = new Tauler(1, true, true, true);
        for (String name : fileNames) {
            IO.llegeixTauler(tauler, path + "/" + name);
            System.out.println("-------------- RESULTAT " + name + " ---------------\n");
            IO.print(tauler.getTauler());
            IO.print("\n");
            for (Color col : new Color[]{Color.Negre, Color.Blanc}) {
                IO.print("Color de la fitxa: " + col);
                ArrayList<Pair> movList = Maquina.generaMoviments(tauler, col);
                for (Pair move : movList) {
                    IO.print(move);
                }
                IO.print("");
            }
        }
    }

    private static void testDecidirJugada() throws IOException, CloneNotSupportedException {
        System.out.println("\n********************** TEST DEL MÈTODE testDecidirJugada **********************\n");
        String path = "jocsProves/Maquina/decidirJugada";
        Tauler tauler = new Tauler(1, true, true, true);
        IO.print("Selecciona el cas a provar");
        IO.print("1 -> Tauler inicial, profunditat 1, sense podes, heurística de pesos, juguen les negres");
        IO.print("2 -> Tauler obvi, profunditat 1, sense podes, heurística de pesos, juguen les negres");
        IO.print("3 -> Tauler mig ple, profunditat 3, sense, heurística de pesos, juguen les negres");
        IO.print("4 -> Tauler quasi ple, profunditat 3, amb podes, heurística de pesos, juguen les negres");
        IO.print("5 -> Tauler problemàtic, profunditat 6, sense podes, heurística de pesos, juguen les blanques");
        IO.print("7 -> Comprovar l'augment d'eficiència de fer podes a no fer-les");
        String input = IO.read();
        Pair p = new Pair(-1, -1);
        switch(input) {
            case "1":
                IO.llegeixTauler(tauler, path + "/taulerInicial.txt");
                p = Maquina.decidirJugada(tauler, Color.Negre, 1, false, false);
                break;
            case "2":
                IO.llegeixTauler(tauler, path + "/taulerObvi1.txt");
                p = Maquina.decidirJugada(tauler, Color.Negre, 1, false, false);
                break;
            case "3":
                IO.llegeixTauler(tauler, path + "/taulerMigPle.txt");
                p = Maquina.decidirJugada(tauler, Color.Negre, 3, false, false);
                break;
            case "4":
                IO.llegeixTauler(tauler, path + "/taulerMigPle.txt");
                p = Maquina.decidirJugada(tauler, Color.Negre, 3, true, false);
                break;
            case "5":
                IO.llegeixTauler(tauler, path + "/jugadaProblematica.txt");
                p = Maquina.decidirJugada(tauler, Color.Blanc, 1, false, false);
                IO.printNoBreak("Moviment amb profunditat 1 ");
                IO.print(p);
                IO.llegeixTauler(tauler, path + "/jugadaProblematica.txt");
                p = Maquina.decidirJugada(tauler, Color.Blanc, 6, false, false);
                break;
            case "6":
                IO.llegeixTauler(tauler, path + "/jugadaProblematica.txt");
                p = Maquina.decidirJugada(tauler, Color.Blanc, 1, true, false);
                IO.printNoBreak("Moviment amb profunditat 1 ");
                IO.print(p);
                IO.llegeixTauler(tauler, path + "/jugadaProblematica.txt");
                p = Maquina.decidirJugada(tauler, Color.Blanc, 6, true, false);
                break;
            case "7":
                IO.llegeixTauler(tauler, path + "/taulerMigPle.txt");
                long t1 = System.currentTimeMillis();
                p = Maquina.decidirJugada(tauler, Color.Blanc, 6, false, false);
                long dif = System.currentTimeMillis() - t1;
                IO.print(p);
                IO.print("Temps (ms): " + dif);
                t1 = System.currentTimeMillis();
                p = Maquina.decidirJugada(tauler, Color.Blanc, 6, true, false);
                dif = System.currentTimeMillis() - t1;
                IO.print(p);
                IO.print("Temps (ms): " + dif);
                break;
        }
        if (!input.equals("7")) {
            IO.printNoBreak("Moviment: ");
            IO.print(p);
            testDecidirJugada();
        }
    }
}
