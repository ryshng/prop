package domini.controladors;

import domini.classes.Jugador;
import domini.classes.Partida;
import domini.classes.Tauler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static java.lang.Integer.parseInt;

public class PartidaTest {

    static int id;
    static Jugador jug1;
    static Jugador jug2;
    static Tauler tauler;
    static boolean torn;
    static Partida partida;

    public static void main(String[] args) throws IOException {
        BufferedReader r = new BufferedReader(new FileReader("jocsProves/Partida/jugador.txt"));
        String s = r.readLine();
        jug1 = new Jugador(s);
        jug2 = new Jugador(s+"2");
        r = new BufferedReader(new FileReader("jocsProves/Partida/id.txt"));
        id = parseInt(r.readLine());
        tauler = new Tauler(1, true, true, true);
        IO.llegeixTauler(tauler, "jocsProves/Partida/tauler.txt");
        r = new BufferedReader(new FileReader("jocsProves/Partida/boolean.txt"));
        String b = r.readLine();
        torn = b.equals("true");
        partida = new Partida(id, jug1, jug2, tauler);
        IO.print("\nId:");
        testGetIdPartida();
        IO.print("\nGet jugador N:");
        testGetJugadorN();
        IO.print("\nGet jugador B:");
        testGetJugadorB();
        IO.print("\nGet tauler:");
        testGetTauler();
        IO.print("\nGet acabada:");
        testGetAcabada();
        IO.print("\nGet temps:");
        testGetTemps();
        IO.print("\nGet torn:");
        testGetTorn();
        IO.print("\nCanvia torn:");
        testCanviaTorn();
    }

    private static void testGetIdPartida() {
        System.out.println(partida.getIdPartida());
    }

    private static void testGetJugadorB() {
        System.out.println(partida.getJugadorB().getIdJugador());
    }

    private static void testGetJugadorN() {
        System.out.println(partida.getJugadorN().getIdJugador());
    }

    private static void testGetTauler() {
        IO.print(partida.getTauler().getTauler());
    }

    private static void testGetAcabada() {
        System.out.println(partida.getAcabada());
    }

    private static void testGetTemps() {
        System.out.println(partida.getTemps());
    }

    private static void testGetTorn() {
        System.out.println(partida.getTorn());
    }

    private static void testCanviaTorn() {
        partida.canviaTorn();
        System.out.println(partida.getTorn());
    }

}
