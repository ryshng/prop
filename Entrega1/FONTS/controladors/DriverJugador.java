package domini.controladors;

import domini.classes.Estadistiques;
import domini.classes.Jugador;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class DriverJugador {

    static String idJ;
    static String newIdJ;
    static double puntuacio;
    static int victories;
    static int derrotes;
    static int totalPartides;
    static int fitxesCapturades;
    static int fitxesPerdudes;
    static long tempsJugat;
    static double puntuacio_ranking;
    static Estadistiques st = new Estadistiques();


    public static void testConstructor() throws IOException {
        Jugador j = new Jugador(idJ);
        if (j.getIdJugador().equals(idJ)){
            System.out.println("Exit construcció");
        }
        else System.out.println("Error construcció");

        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) main(new String[]{""});
        else System.out.println("Per tornar escriure: " + "ext");
    }

    public static void testGetIdJugador() throws IOException {
        Jugador jugador = new Jugador(idJ);
        System.out.println("Valor esperat: 505");
        System.out.println("Valor obtingut:");
        System.out.println(jugador.getIdJugador());

        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) testGets();
        else System.out.println("Per tornar escriure: " + "ext");
    }


    public static void testGetStats() throws IOException {
        st.setPuntuacio(puntuacio);
        st.setVictories(victories);
        st.setDerrotes(derrotes);
        st.setTotalPartides(totalPartides);
        st.setFitxesCapturades(fitxesCapturades);
        st.setFitxesPerdudes(fitxesPerdudes);
        st.setTempsJugat(tempsJugat);
        st.setPuntuacio_ranking(puntuacio_ranking);
        System.out.println(st.getPuntuacio());
        System.out.println(st.getVictories());
        System.out.println(st.getDerrotes());
        System.out.println(st.getTotalPartides());
        System.out.println(st.getFitxesCapturades());
        System.out.println(st.getFitxesPerdudes());
        System.out.println(st.getTempsJugat());

        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) testGets();
        else System.out.println("Per tornar escriure: " + "ext");
    }

    public static void testSetIdJugador() throws IOException {
        Jugador jugador = new Jugador(idJ);
        System.out.println("Valor vell: ");
        System.out.println(jugador.getIdJugador());
        System.out.println("Valor nou: ");
        jugador.setIdJugador(newIdJ);
        System.out.println(jugador.getIdJugador());

        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) testSets();
        else System.out.println("Per tornar escriure: " + "ext");
    }

    public static void testGets() throws IOException {
        System.out.println("Has entrat al testeig dels Gets(). Qué vols testejar? (Escull entre 1-3)\n" +
                "1 -> getIdJugador()\n" +
                "2 -> getStats()\n" +
                "3 -> Tornar enrere");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch (input) {
            case "1":
                DriverJugador.testGetIdJugador();
                break;
            case "2":
                DriverJugador.testGetStats();
                break;
            case "3":
                main(new String[]{""});
        }
    }

    public static void testSets() throws IOException {
        System.out.println("Has entrat al testeig dels Sets(). Qué vols testejar? (Escull entre 1-3)\n" +
                "1 -> setIdJugador()\n" +
                "2 -> Tornar enrere");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch (input) {
            case "1":
                DriverJugador.testSetIdJugador();
                break;
            case "2":
                main(new String[]{""});
        }
    }

    public static void main(String[] args) throws IOException {
        System.out.println("Driver classe Jugador.");
        BufferedReader r = new BufferedReader(new FileReader("../jocsProves/Jugador/Basics/jugador.txt"));
        idJ = r.readLine();
        puntuacio = Double.parseDouble(r.readLine());
        victories = Integer.parseInt(r.readLine());
        derrotes = Integer.parseInt(r.readLine());
        totalPartides = Integer.parseInt(r.readLine());
        fitxesCapturades = Integer.parseInt(r.readLine());
        fitxesPerdudes = Integer.parseInt(r.readLine());
        tempsJugat = Long.parseLong(r.readLine());
        BufferedReader r2 = new BufferedReader(new FileReader("../jocsProves/Jugador/Basics/jugador-2.txt"));
        newIdJ = r2.readLine();

        System.out.println("Qué vols testejar? (Escull entre 1-3)\n" +
                "1 -> Constructores\n" +
                "2 -> Getters\n" +
                "3 -> Setters");
        Scanner scanner2 = new Scanner(System.in);
        String input = scanner2.nextLine();
        switch (input) {
            case "1":
                DriverJugador.testConstructor();
                break;
            case "2":
                DriverJugador.testGets();
                break;
            case "3":
                DriverJugador.testSets();
                break;

        }
    }
}
