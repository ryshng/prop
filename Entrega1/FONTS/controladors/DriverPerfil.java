package domini.controladors;

import domini.classes.Perfil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class DriverPerfil {
    static String user;
    static String password;
    static String idJ;
    static String newUser;
    static String newPassword;


    public static void testConstructor() throws IOException {
        Perfil p = new Perfil(idJ, user, password);
        if (p.getUsuari().equals(user) && p.getContrasenya().equals(password)){
            System.out.println("Exit en la constructora");
        }
        else System.out.println("Error en la constructora");
        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) main(new String[]{""});
        else System.out.println("Per tornar escriure: " + "ext");
    }

    public static void testGetUsuari() throws IOException {
        Perfil perfil = new Perfil(idJ, user, password);
        System.out.println("Valor esperat: unknown");
        System.out.println("Valor obtingut:");
        System.out.println(perfil.getUsuari());
        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) testGets();
        else System.out.println("Per tornar escriure: " + "ext");

    }

    public static void testGetContrasenya() throws IOException {
        Perfil perfil = new Perfil(idJ, user, password);
        System.out.println("Valor esperat: prop123");
        System.out.println("Valor obtingut: ");
        System.out.println(perfil.getContrasenya());
        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) testGets();
        else System.out.println("Per tornar escriure: " + "ext");
    }

    public static void testGetIdJugador() throws IOException {
        Perfil perfil = new Perfil(idJ, user, password);
        System.out.println("Valor esperat: 500");
        System.out.println("Valor obtingut: ");
        System.out.println(perfil.getIdJugador());
        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) testGets();
        else System.out.println("Per tornar escriure: " + "ext");
    }

    public static void testSetUsuari() throws IOException {
        Perfil perfil = new Perfil(idJ, user, password);
        System.out.println("Valor vell: ");
        System.out.println(perfil.getUsuari());
        System.out.println("Valor nou: ");
        perfil.setUsuari(newUser);
        System.out.println(perfil.getUsuari());
        System.out.println("Per tornar escriure: " + "ext");

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) testSets();
        else System.out.println("Per tornar escriure: " + "ext");
    }

    public static void testSetContrasenya() throws IOException {
        Perfil perfil = new Perfil(idJ, user, password);
        System.out.println("Valor vell: ");
        System.out.println(perfil.getContrasenya());
        System.out.println("Valor nou: ");
        perfil.setContrasenya(newPassword);
        System.out.println(perfil.getContrasenya());
        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) testSets();
        else System.out.println("Per tornar escriure: " + "ext");
    }

    public static void testGets() throws IOException {
        System.out.println("Has entrat al testeig dels Gets(). Qué vols testejar? (Escull entre 1-4)\n" +
                "1 -> getUsuari()\n" +
                "2 -> getContrasenya()\n" +
                "3 -> getIdJugador()\n" +
                "4 -> Tornar enrere");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch (input) {
            case "1":
                DriverPerfil.testGetUsuari();
                break;
            case "2":
                DriverPerfil.testGetContrasenya();
                break;
            case "3":
                DriverPerfil.testGetIdJugador();
                break;
            case "4":
                main(new String[]{""});
        }
    }

    public static void testSets() throws IOException {
        System.out.println("Has entrat al testeig dels Sets(). Qué vols testejar? (Escull entre 1-3)\n" +
                "1 -> setUsuari()\n" +
                "2 -> setContrasenya()\n" +
                "3 -> Tornar enrere");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch (input) {
            case "1":
                DriverPerfil.testSetUsuari();
                break;
            case "2":
                DriverPerfil.testSetContrasenya();
                break;
            case "3":
                main(new String[]{""});
        }
    }
    public static void main(String[] args) throws IOException {
        System.out.println("Driver classe Perfil.");
        BufferedReader r = new BufferedReader(new FileReader("../jocsProves/Perfil/Basics/perfil.txt"));
        idJ = r.readLine();
        user = r.readLine();
        password = r.readLine();
        BufferedReader r2 = new BufferedReader(new FileReader("../jocsProves/Perfil/Basics/perfil-2.txt"));
        newUser = r2.readLine();
        newPassword = r2.readLine();
        System.out.println("Qué vols testejar? (Escull entre 1-3)\n" +
                            "1 -> Constructores\n" +
                            "2 -> Getters\n" +
                            "3 -> Setters");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch (input) {
            case "1":
                DriverPerfil.testConstructor();
                break;
            case "2":
                DriverPerfil.testGets();
                break;
            case "3":
                DriverPerfil.testSets();
                break;

        }

    }
}
