package domini.controladors;

import domini.shared.Color;

import static domini.controladors.IO.print;


public class DriverHeuristica {
    public static void posEvalTest1() throws IOException {
        Tauler tau = new Tauler(1, true, true, true);
        Heuristica heu = new Heuristica();
        IO.llegeixTauler(tau, "../jocsProves/Heuristica/Heuristica1/test1-heu1.txt");
        //print(tau.getTauler());
        int value = Heuristica.heuristic1(tau, Color.Negre);
        System.out.println("-Valor esperat: n > 0");
        System.out.printf("-Valor calculat: %d\n", value);

        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) heuristic1Test();
        else System.out.println("Per tornar escriure: " + "ext");

    }

    public static void negEvalTest1() throws IOException {
        Tauler tau = new Tauler(1, true, true, true);
        Heuristica heu = new Heuristica();
        IO.llegeixTauler(tau, "../jocsProves/Heuristica/Heuristica1/test2-heu1.txt");
        int value = Heuristica.heuristic1(tau, Color.Negre);
        System.out.println("-Valor esperat: n < 0");
        System.out.printf("-Valor calculat: %d\n", value);

        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) heuristic1Test();
        else System.out.println("Per tornar escriure: " + "ext");

    }
    public static void posEvalTest2() throws IOException {
        Tauler tau = new Tauler(1, true, true, true);
        Heuristica heu = new Heuristica();
        IO.llegeixTauler(tau, "../jocsProves/Heuristica/Heuristica2/test1-heu2.txt");
        int value = Heuristica.heuristic2(tau, Color.Negre);
        System.out.println("-Valor esperat: n > 0");
        System.out.printf("-Valor calculat: %d\n", value);

        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) heuristic2Test();
        else System.out.println("Per tornar escriure: " + "ext");
    }

    public static void negEvalTest2() throws IOException {
        Tauler tau = new Tauler(1, true, true, true);
        Heuristica heu = new Heuristica();
        IO.llegeixTauler(tau, "../jocsProves/Heuristica/Heuristica2/test2-heu2.txt");
        int value = Heuristica.heuristic2(tau, Color.Negre);
        System.out.println("-Valor esperat: n < 0");
        System.out.printf("-Valor calculat: %d\n", value);

        System.out.println("Per tornar escriure: " + "ext");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        if (input.equals("ext")) heuristic2Test();
        else System.out.println("Per tornar escriure: " + "ext");
    }

    public static void heuristic1Test() throws IOException{
        System.out.println( "Escull un dels dos casos:\n" +
                            "1 -> Avaluacio positiva\n" +
                            "2 -> Avaluacio negativa\n" +
                            "3 -> Tornar enrere");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch (input) {
            case "1":
                DriverHeuristica.posEvalTest1();
                break;
            case "2":
                DriverHeuristica.negEvalTest1();
                break;
            case "3":
                main(new String[]{""});
        }
    }
    public static void heuristic2Test() throws IOException {
        System.out.println( "Escull un dels dos casos:\n" +
                "1 -> Avaluacio positiva\n" +
                "2 -> Avaluacio negativa\n" +
                "3 -> Tornar enrere");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch (input) {
            case "1":
                DriverHeuristica.posEvalTest2();
                break;
            case "2":
                DriverHeuristica.negEvalTest2();
                break;
            case "3":
                main(new String[]{""});
        }
    }


    public static void main(String[] args) throws IOException {
        System.out.println("Driver classe Heurisitca.");
        System.out.println("Qué vols testejar? (Escull entre 1-2)\n" +
                "1 -> Heuristica1\n" +
                "2 -> Heuristica2");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        switch (input) {
            case "1":
                DriverHeuristica.heuristic1Test();
                break;
            case "2":
                DriverHeuristica.heuristic2Test();
                break;
        }

    }
}
