package domini.controladors;

import domini.classes.CASELLA;
import domini.shared.Color;

import java.io.IOException;

public class CASELLAtests {
    public static void main(String[] args) throws IOException {
        testConstructora();
        testgetcolor();
        testsetcolor();
        testvoltejable();
        testvoltejar();

    }

    private static void testConstructora() throws IOException {
        CASELLA c = new CASELLA();
        if(c.getcolor() == Color.Buit) System.out.println("La casella ha sigut creada correctament");
        else System.out.println("La casella ha sigut creada correctament");
    }

    private  static void testgetcolor() throws IOException {
        CASELLA c = new CASELLA();
        if(c.getcolor() == Color.Buit) System.out.println("Retorna correctament el color");
        else System.out.println("No retorna correctament el color");
    }

    private  static void testsetcolor() throws IOException {
        CASELLA c = new CASELLA();
        c.setcolor(Color.Negre);
        if(c.getcolor() == Color.Negre) System.out.println("l'atribut color ha sigut modificat correctament");
        else System.out.println("Ho sento, l'atribut color no s'ha modificat correctament");
    }

    private  static void testvoltejable() throws IOException {
        CASELLA c = new CASELLA();
        c.setcolor(Color.Blanc);
        if(c.voltejable(Color.Negre)) System.out.println("La casella és voltejable per una negra però no per cap altre " +
                "tipus de color");
        else System.out.println("Ho sento, la casella no és voltejable");
    }

    private  static void testvoltejar() throws IOException {
        CASELLA c = new CASELLA();
        c.setcolor(Color.Blanc);
        c.voltejar();
        if(c.getcolor() == Color.Negre) System.out.println("La casella ha sigut voltejada correctament");
        else System.out.println("Ho sento, la casella no ha sigut voltejada correctament");
    }
}
