package domini.Test;

import domini.classes.Estadistiques;
import domini.classes.Perfil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class EstadistiquesTest {

    Estadistiques stats;

    @Before
    public void setUp() {
        stats = new Estadistiques();
    }

    public static void main(String[] args) throws IOException {
        System.out.println("Test JUnit classe Estadistiques.");
        System.out.println("Test constructora.");
        System.out.println("Test getters & setters :");


    }
    @Test
    public  void test_Estadistiques(){

        Assert.assertNotNull("Creador noves Estadistiques failed", stats);
    }

    @Test
    public void test_setTotalPartides() {

        int x = 3;
        stats.setTotalPartides(x);
        Assert.assertEquals(stats.getTotalPartides(),x);
        System.out.println("Resultat total de Partides es igual "+x+" = " +stats.getTotalPartides());
    }

    @Test
    public  void test_setFitxesCapturades() {

        int x = 4;
        stats.setFitxesCapturades(x);
        Assert.assertEquals(stats.getFitxesCapturades(),x);
        System.out.println("Resultat fitxes capturades es igual  "+x+" = " +stats.getFitxesCapturades());
    }

    @Test
    public void test_setFitxesPerdudes() {

        int x = 3;
        stats.setFitxesPerdudes(x);
        Assert.assertEquals(stats.getFitxesPerdudes(),x);
        System.out.println("Resultat de fitxes perdudes  es igual  "+x+" = " +stats.getFitxesPerdudes());
    }

    @Test
    public void setDerrotes() {

        int x = 10;
        stats.setDerrotes(x);
        Assert.assertEquals(stats.getDerrotes(),x);
        System.out.println("Resultat de Derrotes es igual " +x+ " = " + stats.getDerrotes());
    }

    @Test
    public void test_getDerrotes() {
        Integer x = stats.getDerrotes();
        Assert.assertNotNull("Error: Estadistica de les  derrotes del jugador es null ",x);
        System.out.println(x);
    }

    @Test
    public void test_getPuntuacio() {
        Assert.assertNotNull("Error: Estadistica de puntuacio del jugador es null ", (Double)stats.getPuntuacio());
    }

    @Test
    public void test_getPuntuacio_ranking() {
        Assert.assertNotNull("Error: Estadistica de puntuacio del jugador (Perfil)'no maquina' es null ", (Double)stats.getPuntuacio_ranking());
    }

    @Test
    public void test_setPuntuacio() {
        Double x = 100.0;
        stats.setPuntuacio(x);
        Assert.assertEquals(((Double)stats.getPuntuacio()), x);
        System.out.println("Resultat de Puntuacio es igual " +x+ " = " + stats.getPuntuacio());
    }

    @Test
    public void test_getVictories() {
        Integer x = stats.getVictories();
        Assert.assertNotNull("Error: Estadistica de les  victories del jugador es null ",x);
    }

    @Test
    public void test_setVictories() {
        int x = 10;
        stats.setVictories(x);
        Assert.assertEquals(stats.getVictories(),x);
        System.out.println("Resultat totalPartides funciona \n  "+x+" = " +stats.getVictories());

    }

    @Test
    public void test_getTotalPartides() {
        Integer x = stats.getTotalPartides();
        Assert.assertNotNull("Error: Estadistica de les  partides totals del jugador es null ",x);
    }

    @Test
    public void test_getFitxesCapturades() {
        Integer x = stats.getFitxesCapturades();
        Assert.assertNotNull("Error: Estadistica de les  fitxes capturades del jugador es null ",x);
    }

    @Test
    public void test_getFitxesPerdudes() {
        Integer x = stats.getFitxesPerdudes();
        Assert.assertNotNull("Error: Estadistica de les  fitxes perdudes del jugador es null ",x);
    }

    @Test
    public void Test_calculPuntuacio() {
        stats = new Estadistiques();
        Perfil j1 = new Perfil("James");
        Perfil j2 = new Perfil("Raimon");
        boolean guanyat = true;
        int fitxesCapturades = 10;
        int fitxesPerdudes = 7;
        String regles = "HV";

        Double r_esperat = 110.0;
        Double res =stats.CalculPuntuacio(j1,j2,guanyat,fitxesCapturades,fitxesPerdudes,regles);
        System.out.println("El resultat concorda " + res + " = " + r_esperat );
        Assert.assertEquals("No concorda amb el resultat /n",r_esperat ,res);

    }


    @Test
    public void Test_actualitzar_estadistiques(){
        stats = new Estadistiques(); //supongo que la variables se inicializan a 0;
        boolean T = true;
        Double puntuacio_final = 100.0;

        boolean victoria = true;
        int f_capt = 10;
        int f_perd = 2;
        stats.actualitzar_estadistiques(T,puntuacio_final, victoria, f_capt,f_perd);
        Assert.assertEquals("No concorda amb el resultat /n", (Double)stats.getPuntuacio(), puntuacio_final);
        System.out.println("El resultat concorda " + puntuacio_final + " = " + (Double)stats.getPuntuacio() );
    }
}