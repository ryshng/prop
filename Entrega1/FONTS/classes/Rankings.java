package domini.classes;


public class Rankings {


    private int posicio;

    Map<String, Double> hashmapP; //HashMap de Huma vs Huma

    Map<String, Double> hashmapT; //maq vs maq





    /**
     * Constructora per defecte
     */
    public Rankings() {
        hashmapP=new TreeMap<>();
        hashmapT=new TreeMap<>();
    }

    /*GETTERS I SETTERS */

    public Map<String,Double> getrankingP(){
        return hashmapP;
    }

    public Map<String,Double> getrankingT(){
        return hashmapT;
    }

    public int getPosicio() {
        return this.posicio;
    }

    public void setPosicio(int posicio) {
        this.posicio = posicio;
    }




    //METODES//


    /**
     * Crear una instancia nova de Jugador(Màquina o Humà) amb puntuació:0.0 al ranking corresponent.
     * @param idJugador : id del judador
     */

    public void afegir_jugador(Jugador idJugador) {
        String nom = idJugador.getIdJugador();
        if (idJugador instanceof Perfil) {
            hashmapP.putIfAbsent(nom, 0.0);
        }
            hashmapT.put(nom, 0.0);
    }

    /**
     * Actualitzar puntuació del idJugador depenent si es Humà o Màquina
     * @param idJugador: id del jugador
     */
    public void actualitzar_puntuacio(Jugador idJugador) {
        String nom = idJugador.getIdJugador();
        if (idJugador instanceof Perfil) {
            hashmapP.put(nom, idJugador.getStats().getPuntuacio_ranking());//Afegir puntuacio d'un jugador nou.
        }
        hashmapT.put(nom, idJugador.getStats().getPuntuacio_ranking()); //Afegir Puntuacio ranquing maquina
    }
}
