package domini.classes;

import domini.shared.Color;

public class Partida {

    /* ----- ATRIBUTS ----- */

    private final int idPartida;

    private final Jugador jugadorN;

    private final Jugador jugadorB;

    private final Tauler tauler;

    private boolean acabada;

    private final long temps;

    private boolean torn;

    private final Color guanyador;

    /* ----- MÈTODES ----- */

    /**
     * Constructora per defecte
     */

    public Partida(int idPartida, Jugador jugadorN, Jugador jugadorB, Tauler tauler) {
        this.idPartida = idPartida;
        this.jugadorN = jugadorN;
        this.jugadorB = jugadorB;
        this.tauler = tauler;
        this.acabada = false;
        this.temps = System.currentTimeMillis();
        this.torn = true;
        this.guanyador = Color.Buit;
    }

    public Partida(int idPartida, Maquina jugadorN, Maquina jugadorB, Tauler tauler) {
        this.idPartida = idPartida;
        this.jugadorN = jugadorN;
        this.jugadorB = jugadorB;
        this.tauler = tauler;
        this.acabada = false;
        this.temps = System.currentTimeMillis();
        this.torn = true;
        this.guanyador = Color.Buit;
    }

    public Partida(int idPartida, Jugador jugadorN, Maquina jugadorB, Tauler tauler) {
        this.idPartida = idPartida;
        this.jugadorN = jugadorN;
        this.jugadorB = jugadorB;
        this.tauler = tauler;
        this.acabada = false;
        this.temps = System.currentTimeMillis();
        this.torn = true;
        this.guanyador = Color.Buit;
    }

    public int getIdPartida() {
        return this.idPartida;
    }

    public Jugador getJugadorB() {
        return jugadorB;
    }

    public Jugador getJugadorN() {
        return jugadorN;
    }

    public Tauler getTauler() {
        return this.tauler;
    }

    public boolean getAcabada() {
        return acabada;
    }

    public void getAcabada(boolean acab) {
        acabada = acab;
    }

    public long getTemps() {
        return this.temps;
    }

    public boolean getTorn() {
        return torn;
    }

    public void canviaTorn() {
        this.torn = !this.torn;
    }

}
