package domini.classes;

public class Tauler implements Cloneable {
        private int idTauler;
        private int fitxesB;
        private int fitxesN;
        private CASELLA[][] tauler;
        //regles pel tauler
        private boolean horitzontal;
        private boolean vertical;
        private boolean diagonals;

        //Constructores

        /**
         * Constructora de tauler per defecte, el tauler s'inicialitza amb el tauler típic de l'Othello clàsc,
         * amb totes regles de captura(horitzonta, vertical i diagonals) i amb el seu identificador
         * @param idtauler identificador del tauler que es creat
         */
        public Tauler (int idtauler){
                this.idTauler = idtauler;
                this.fitxesB = 2;
                this.fitxesN = 2;
                this.tauler = new CASELLA[8][8];
                // Creació de le caselles del tauler a Buit
                for (int i = 0; i < 8; i++)
                        for(int j = 0; j < 8; j++)
                                tauler[i][j] = new CASELLA();
                this.horitzontal = true;
                this.vertical = true;
                this.diagonals = true;
                iniTaulell();
        }

        /**
         * Constructora de tauler que inicialitza les fitxes inicials del tauler com el de l'Otello clàsic i
         * té com a regles de captura les que donen com a paràmetres
         * @param idtauler identificador del tauler que es creat
         * @param h boolea que indica si les regles de captura en horitontal del tauler creat (true si es pot, false si no)
         * @param v boolea que indica si les regles de captura en vertical del tauler creat (true si es pot, false si no)
         * @param d boolea que indica si les regles de captura en diagonal del tauler creat (true si es pot, false si no)
         */
        public Tauler (int idtauler, boolean h, boolean v, boolean d){// Pre: si h = v = d = false, no és pot fer
                this.idTauler = idtauler;
                this.fitxesB = 2;
                this.fitxesN = 2;
                this.tauler = new CASELLA[8][8];
                // Creació de le caselles del tauler a Buit
                for (int i = 0; i < 8; i++)
                        for(int j = 0; j < 8; j++)
                                tauler[i][j] = new CASELLA();
                this.horitzontal = h;
                this.vertical = v;
                this.diagonals = d;
                iniTaulell();
        }

        /**
         * Mètode per obtenir una hard copy d'un tauler
         * @return Retorna una còpia d'un tauler
         * @throws CloneNotSupportedException Si no es pot fer un clon de l'objecte
         */
        public Tauler clone() throws CloneNotSupportedException {
                Tauler clon = (Tauler)super.clone();
                clon.tauler = new CASELLA[8][8];
                for (int i = 0; i < 8; ++i)
                        for (int j = 0; j < 8; ++j) {
                                clon.tauler[i][j] = new CASELLA(tauler[i][j].getcolor());
                        }
                return clon;
        }

        /**
         * Inicialitza el tauler amb les fitxes inicials per defecte, típic de l'Othello clàsic.
         */
        private void iniTaulell() { // afegeix les 4 fitxes inicials al tauler
                tauler[3][3].color = Color.Negre;
                tauler[4][4].color = Color.Negre;
                tauler[3][4].color = Color.Blanc;
                tauler[4][3].color = Color.Blanc;
        }

        //gets
        public int getIdTauler(){
                return this.idTauler;
        }

        public int getFitxesB(){ return this.fitxesB;}
        public int getFitxesN(){ return this.fitxesN;}

        public CASELLA[][] getTauler(){ return this.tauler; }

        public boolean getHoritzontal(){return this.horitzontal;}
        public boolean getVertical(){return this.vertical;}
        public boolean getDiagonals(){return this.diagonals;}

        //sets
        public void setIdTauler(int id){idTauler = id;}
        public void setTauler(CASELLA[][] taulernou){this.tauler = taulernou;} // al fer això, qui crida la funció és
        // l'encarregat de que hi hagi coherència entre fitxesB i fitxesN
        public void setHoritzontal(boolean h){horitzontal = h;}
        public void setVertical(boolean v){vertical = v;}
        public void setDiagonals(boolean d){diagonals = d;}
        public void setFitxesB(int b){fitxesB = b;}
        public void setFitxesN(int n){fitxesN = n;}

        // altres mètodes

        // Els metodes segúents son per mirar que una fitxa de color col pugui capturar

        /**
         * Mira si pot capturar la part horitzontal esquerra de la fitxa que volem possar.
         * @param pos posició de la fitxa a possar
         * @param col color de la fitxa a possar
         * @param c_adversari color de l'oponent
         * @return retorna true si es pot capturar la part horitzontal esquerre de la fitxa amb color col i posició pos,
         * sinó retorna false
         */
        private boolean Esquerra_valida(Pair pos, Color col, Color c_adversari) {
                boolean capturable = false;
                int i = pos.getKey();
                int j = pos.getValue();
                if(j >= 2 && tauler[i][j - 1].color == c_adversari) {//si almenys hi ha dos caselles més a la seva
                        // esquerra si just la de la fitxa de la seva esquerra es del color contrari, pot ser capturable
                        j -= 2;
                        while (j >= 0 && !capturable) {
                                if (this.tauler[i][j].getcolor() == col) capturable = true;// si arriba un moment que hi ha una fitxa del mateix color que volem possar, és capturable
                                else if(tauler[i][j].getcolor() == Color.Buit) j = -1; // no es podrà capturar
                                --j;
                        }
                }
                return capturable;
        }

        /**
         * Mira si pot capturar la part horitzontal dreta de la fitxa que volem possar.
         * @param pos posició de la fitxa a possar
         * @param col color de la fitxa a possar
         * @param c_adversari color de l'oponent
         * @return retorna true si es pot capturar la part horitzontal dreta de la fitxa amb color col i posició pos,
         * sinó retorna false
         */
        private boolean Dreta_valida(Pair pos, Color col, Color c_adversari){
                boolean capturable = false;
                //si no fa el return, capturable = false
                int i = pos.getKey();
                int j = pos.getValue();
                //mirem si podem capturar la part de la dreta de la fitxa que volem possar
                if(j <= 5 && tauler[i][j+1].color == c_adversari) { // si just la de la fitxa de la seva dreta es del color contrari, pot ser capturable
                        j += 2;
                        while (j <= 7 && !capturable) {
                                if (this.tauler[i][j].getcolor() == col) capturable = true;// si arriba un moment que hi ha una fitxa del mateix color que volem possar, és capturable
                                else if (tauler[i][j].getcolor() == Color.Buit) j = 9; //si arriba un moment que no hi ha fitxa, no es podrà capturar
                                ++j;
                        }
                }
                return capturable;
        }

        /**
         * Si les regles deixen capturar horitzontalment, mira si es pot capturar en horitzontal al possar la fitxa de color col en la posició pos
         * @param pos posició en la que es vol posar la fitxa
         * @param col color de la fitxa a possar
         * @param c_adversari color de l'oponent
         * @return retorna -1 si no es possible capturar horitzontalment, sinó, retorna 0 si pot capturar la part de la
         * dreta i 1 si la part de l'esquerra no es podia però la de la dreta si
         */
        private int Hvalid(Pair pos, Color col, Color c_adversari) {
                //mira si es pot capturar en horitzontal
                if(Esquerra_valida(pos, col, c_adversari)) return 0;
                if(Dreta_valida(pos, col, c_adversari)) return 1;
                return -1;
        }

        /**
         * Mira si pot capturar la part vertical de sobre de la fitxa que volem possar.
         * @param pos posició en la que es vol posar la fitxa
         * @param col color de la fitxa a possar
         * @param c_adversari color de l'oponent
         * @return retorna true si es pot capturar la part vertical de sobre de la fitxa amb color col i posició pos,
         * sinó retorna false
         */
        private boolean a_sobre_valid(Pair pos, Color col, Color c_adversari) {
                boolean capturable = false;
                int i = pos.getKey();
                int j = pos.getValue();
                if(i >= 2 && tauler[i-1][j].color == c_adversari) {// si almenys hi ha dos caselles més a sobre i just
                        // la de la fitxa de sota és del color contrari, pot ser capturable
                        i -= 2;
                        while (i >= 0 && !capturable) {
                                if (this.tauler[i][j].getcolor() == col) capturable = true;// si arriba un moment que hi ha una fitxa del mateix color que volem possar, és capturable
                                else if(tauler[i][j].getcolor() == Color.Buit) i = -1; // no es podrà capturar
                                --i;
                        }
                }
                return capturable;
        }

        /**
         * Mira si pot capturar la part de vertical sota de la fitxa que volem possar.
         * @param pos posició en la que es vol posar la fitxa
         * @param col color de la fitxa a possar
         * @param c_adversari color de l'oponent
         * @return retorna true si es pot capturar la part vertical de sota de la fitxa amb color col i posició pos,
         * sinó retorna false
         */
        private boolean a_sota_valid(Pair pos, Color col, Color c_adversari) {
                //mirem si podem capturar la part de la dreta de sota
                boolean capturable = false;
                int i = pos.getKey();
                int j = pos.getValue();
                if(i <= 5 && tauler[i+1][j].color == c_adversari) { //si almenys hi ha dos caselles més a sota i si just
                        // la de la fitxa de sota es del color contrari, pot ser capturable
                        i += 2;
                        while (i <= 7 && j <= 7 && !capturable) {
                                if (this.tauler[i][j].getcolor() == col) capturable = true;// si arriba un moment que
                                        // hi ha una fitxa del mateix color que volem possar, és capturable
                                else if (tauler[i][j].getcolor() == Color.Buit) i = 9; // no es podrà capturar
                                ++i;
                        }
                }
                return capturable;
        }

        /**
         * Si les regles deixen capturar verticalment, mira si es pot capturar en vertical al possar la fitxa de color col en la posició pos
         * @param pos posició en la que es vol posar la fitxa
         * @param col color de la fitxa a possar
         * @param c_adversari color de l'oponent
         * @return retorna -1 si no es posible capturar la fitxa de color col en la posició pos. Sinó,
         * retorna 2 si la part de sota és capturable o 3 si la part de sota no és capturable però la part de dalt sí.
         */
        private int Vvalid(Pair pos, Color col, Color c_adversari) {
                if(a_sota_valid(pos, col, c_adversari)) return 2;
                if(a_sobre_valid(pos, col, c_adversari)) return 3;
                return -1;
        }

        /**
         * Mira si pot capturar la diagonal superior esquerra de la fitxa que volem possar.
         * @param pos posició en la que es vol posar la fitxa
         * @param col color de la fitxa a possar
         * @param c_adversari color de l'oponent
         * @return retorna true si es pot capturar la diagonal superior esquerra de la fitxa amb color col i posició pos,
         * sinó retorna false
         */
        private boolean superior_esquerre_valid(Pair pos, Color col, Color c_adversari){
                //mirem si podem capturar la diagonal superior esquerre de la fitxa que volem possar \
                boolean capturable = false;
                int i = pos.getKey();
                int j = pos.getValue();
                if(i >= 2 && j >= 2 && tauler[i-1][j-1].color == c_adversari) {//si almenys hi ha dos caselles més en la
                        // part superior esquerra i si just la de la fitxa superior esquerra és del color contrari, pot ser capturable
                        i -= 2;
                        j -= 2;
                        while (i >= 0 && j >= 0 && !capturable) {
                                if (this.tauler[i][j].getcolor() == col) capturable = true;// si arriba un moment que hi ha una fitxa del mateix color que volem possar, és capturable
                                else if(tauler[i][j].getcolor() == Color.Buit) i = -1; // no es podrà capturar
                                --i;
                                --j;
                        }
                }
                return capturable;
        }

        /**
         * Mira si pot capturar la diagonal superior dreta de la fitxa que volem possar.
         * @param pos posició en la que es vol posar la fitxa
         * @param col color de la fitxa a possar
         * @param c_adversari color de l'oponent
         * @return retorna true si es pot capturar la diagonal superior dreta de la fitxa amb color col i posició pos,
         * sinó retorna false
         */
        private boolean superior_dreta_valid(Pair pos, Color col, Color c_adversari){
                boolean capturable = false;
                int i = pos.getKey();
                int j = pos.getValue();
                if(i >= 2 && j <= 5 && tauler[i-1][j+1].color == c_adversari) { // si almenys hi ha dos caselles més en
                        // la part superior dreta i si just la de la fitxa superior dreta és del color contrari, pot ser capturable
                        i -= 2;
                        j += 2;
                        while (i >= 0 && j <= 7 && !capturable) {
                                if (this.tauler[i][j].getcolor() == col) capturable = true;// si arriba un moment que hi ha una fitxa del mateix color que volem possar, és capturable
                                else if (tauler[i][j].getcolor() == Color.Buit) i = -1; // no es podrà capturar
                                --i;
                                ++j;
                        }
                }
                return capturable;
        }

        /**
         * Mira si pot capturar la diagonal inferior esquerra de la fitxa que volem possar.
         * @param pos posició en la que es vol posar la fitxa
         * @param col color de la fitxa a possar
         * @param c_adversari color de l'oponent
         * @return retorna true si es pot capturar la diagonal inferior esquerra de la fitxa amb color col i posició pos,
         * sinó retorna false
         */
        private boolean inferior_esquerre_valid(Pair pos, Color col, Color c_adversari) {
                boolean capturable = false;
                int i = pos.getKey();
                int j = pos.getValue();
                if(j >= 2 && i <= 5 && tauler[i+1][j-1].color == c_adversari) {// si just la diagonal inferior esquerra
                        // de la fitxa és del color contrari, pot ser capturable
                        i += 2;
                        j -= 2;
                        while (i <= 7 && j >= 0 && !capturable) {
                                if (this.tauler[i][j].getcolor() == col) capturable = true;// si arriba un moment que hi ha una fitxa del mateix color que volem possar, és capturable
                                else if(tauler[i][j].getcolor() == Color.Buit) i = 8; // no es podrà capturar
                                ++i;
                                --j;
                        }
                }
                return capturable;
        }

        /**
         * Mira si pot capturar la diagonal inferior dreta de la fitxa que volem possar.
         * @param pos posició en la que es vol posar la fitxa
         * @param col color de la fitxa a possar
         * @param c_adversari color de l'oponent
         * @return retorna true si es pot capturar la diagonal inferior dreta de la fitxa amb color col i posició pos,
         * sinó retorna false
         */
        private boolean inferior_dreta_valid(Pair pos, Color col, Color c_adversari) {
                boolean capturable = false;
                int i = pos.getKey();
                int j = pos.getValue();
                if(i <= 5 && j <= 5 && tauler[i+1][j+1].color == c_adversari) { // si almenys hi ha 2 caselles en la
                        // diagonal inferior dreta i just la de la fitxa de la diagonal inferior dreta es del color
                        // contrari, pot ser capturable
                        i += 2;
                        j += 2;
                        while (i <= 7 && j <= 7 && !capturable) {
                                if (this.tauler[i][j].getcolor() == col) capturable = true;// si arriba un moment que hi ha una fitxa del mateix color que volem possar, és capturable
                                else if (tauler[i][j].getcolor() == Color.Buit) i = 8; // no es podrà capturar
                                ++i;
                                ++j;
                        }
                }
                return capturable;
        }

        /**
         * Si les regles deixen capturar diagonalment, mira si es pot capturar en diagonal al possar la fitxa de color col en la posició pos
         * @param pos posició en la que es vol posar la fitxa
         * @param col color de la fitxa a possar
         * @param c_adversari color de l'oponent
         * @return retorna -1 si no es posible capturar cap diagonals. En cas de que es pugui, retorna 4 si pot capturar
         * la diagonal superior esquerre, 5 si l'anterior no ha pogut però si la diagonal superior dreta
         * 6 si les anteriors no ha pogut capturar, però si la diagonal inferior esquerra,  7 les anteriors no s'han
         * capturat però la diagonal inferior dreta si
         */
        private int Dvalides(Pair pos, Color col, Color c_adversari){
                if(superior_esquerre_valid(pos, col, c_adversari)) return 4;
                else if (superior_dreta_valid(pos, col, c_adversari)) return 5;
                else if(inferior_esquerre_valid(pos, col, c_adversari)) return 6;
                else if(inferior_dreta_valid(pos, col, c_adversari)) return 7;
                return -1;
        }

        /**
         * Mira si al possar una fitxa de color col en la posició p, amb les regles del tauler es pot capturar almenys una fitxa
         * @param p posició en la que es vol posar la fitxa
         * @param col color de la fitxa a possar
         * @return retorna un enter entre -1 i 7 depenent de fins on s'hagi evaluat la captura de la fitxa, on cada
         * número representa un predicat:
         *                                          - 0: es posible capturar la part esquerra
         *                                          - 1: es posible capturar la part dreta
         *                                          - 2: es posible capturar la part de sota
         *                                          - 3: es posible capturar la part de sobre
         *                                          - 4: es poible capturar la diagonal superior esquerra
         *                                          - 5: es poible capturar la diagonal superior dreta
         *                                          - 6: es poible capturar la diagonal inferior esquerra
         *                                          - 7: es poible capturar la diagonal inferior dreta
         *                                          - -1: no es posible fer captures
         *           Si retorna un valor x on -1 <= x <= 7, vol dir que els predicats amb un número inferior són falsos.
         *           Exemple: si retorna 1, vol dir que es posible capturar la part esquerra(1 cert), però no la part
         *           dreta (0 fals) i per tant es posible fer captures(-1 fals).
         */
        private int capturable(Pair p, Color col){ // retorna si es posible capturar
                //Obtenim el color del adversari
                Color c_adversari;
                if(col == Color.Blanc) c_adversari = Color.Negre;
                else  c_adversari = Color.Blanc;

                int estat = -1;
                if(horitzontal) estat = Hvalid(p, col, c_adversari);
                if(estat != -1) return estat;
                if(vertical) estat = Vvalid(p, col, c_adversari);
                if(estat != -1) return estat;
                if(diagonals) estat = Dvalides(p, col, c_adversari);
                return estat;
        }

        /**
         * actualitza les fitxes negres i blanques que hi ha al tauler quan es volteja una casella
         * @param col color de la fitxa que captura, és a dir de qui té el torn per posar la seva fitxa en el tauler
         */
        private void actualitza_fitxes(Color col) {
                if(col == Color.Negre) {
                        ++fitxesN;
                        --fitxesB;
                }
                else {
                        ++fitxesB;
                        --fitxesN;
                }
        }

        /**
         * Pre: la fitxa de color col al posar-la en la posició pos, ha de capturar almenys 1 fitxa.
         * Captura totes les fitxes que pugui suposant que s'ha posat una fitxa de color col en la posició pos i
         * actualitza els atributs de tauler.
         * @param pos posició en la que es vol posar la fitxa
         * @param col color de la fitxa a posar
         * @param estat indica el predicat anomenat estat en el que es troba el procés de captura, on els predicats són:
         *                            - estat = 0: es posible capturar la part esquerra
         *                            - estat = 1: es posible capturar la part dreta
         *                            - estat = 2: es posible capturar la part de sota
         *                            - estat = 3: es posible capturar la part de sobre
         *                            - estat = 4: es posible capturar la diagonal superior esquerra
         *                            - estat = 5: es posible capturar la diagonal superior dreta
         *                            - estat = 6: es posible capturar la diagonal inferior esquerra
         *                            - estat = 7: es posible capturar la diagonal inferior dreta
         *              És compleix que: - l'estat només pot pendre enters del 0 al 7
         *                               - Un estat x vol dir que el predicat asocat a l'estat x és cert, però tots
         *                                 els predicats asosiats a números inferiors a x son falsos. Per exemple:
         *                                 si estat = 2 vol dir que es posible capturar la part de sota, però no es
         *                                 posible capturar la part esquerra (0) ni tampoc la part dreta (1)
         */
        private void captura(Pair pos, Color col, int estat) {
                // calculem el color de l'adversari
                Color c_adversari;
                if(col == Color.Blanc) c_adversari = Color.Negre;
                else  c_adversari = Color.Blanc;

                int j = pos.getValue();
                int i = pos.getKey();
                boolean acabat = false;

                // capturem les horitzontals
                if (horitzontal) {
                        if (estat == 0) { // capturable amb Esquerra_valid
                                // captura la part de l'esquerra
                                --j;
                                while (j >= 0 && !acabat) {
                                        if (this.tauler[i][j].voltejable(col)) { // Si la fitxa é
                                                tauler[i][j].voltejar();
                                                // actualitzem les fitxes del tauler
                                                actualitza_fitxes(col);
                                        } else acabat = true;
                                        --j;
                                }
                        }
                        //capturar la part de la dreta
                        j = pos.getValue();
                        i = pos.getKey();
                        acabat = false;
                        if (estat == 1 || (estat == 0 && Dreta_valida(pos, col, c_adversari))) { // Si l'estat era 1, se sap que
                                //Dreta_valida(pos, col, c_adversari) era certa, i per tant no s'ha de cpmprovar.
                                ++j;
                                while (j <= 7 && !acabat) {
                                        if (this.tauler[i][j].voltejable(col)) {
                                                tauler[i][j].voltejar();
                                                // actualitzem les fitxes del tauler
                                                actualitza_fitxes(col);
                                        } else acabat = true;
                                        ++j;
                                }

                        }
                }
                // capturem les verticals
                if(vertical) {
                        // capturem la part de sota
                        j = pos.getValue();
                        i = pos.getKey();
                        acabat = false;
                        if (estat == 2 || (estat < 2 && a_sota_valid(pos, col, c_adversari))) {
                                ++i;
                                while (j <= 7 && !acabat) {
                                        if (this.tauler[i][j].voltejable(col)) {
                                                tauler[i][j].voltejar();
                                                // actualitzem les fitxes del tauler
                                                actualitza_fitxes(col);
                                        } else acabat = true;
                                        ++i;
                                }
                        }

                        // capturar la part de sobre
                        j = pos.getValue();
                        i = pos.getKey();
                        acabat = false;
                        if (estat == 3 || (estat < 3 && a_sobre_valid(pos, col, c_adversari))) {
                                --i;
                                while (i >= 0 && !acabat) {
                                        if (this.tauler[i][j].voltejable(col)) {
                                                tauler[i][j].voltejar();
                                                // actualitzem les fitxes del tauler
                                                actualitza_fitxes(col);
                                        } else acabat = true;
                                        --i;
                                }
                        }
                }
                // capturem les diagonals
                if(diagonals) {
                        //captura la part superior esquerra
                        j = pos.getValue();
                        i = pos.getKey();
                        acabat = false;
                        if (estat == 4 || (estat < 4 && superior_esquerre_valid(pos, col, c_adversari))) {
                                --i;
                                --j;
                                while (j >= 0 && i >= 0 && !acabat) {
                                        if (this.tauler[i][j].voltejable(col)) {
                                                tauler[i][j].voltejar();
                                                // actualitzem les fitxes del tauler
                                                actualitza_fitxes(col);
                                        } else acabat = true;
                                        --i;
                                        --j;
                                }

                        }

                        // caturem la diagonal superior dreta
                        j = pos.getValue();
                        i = pos.getKey();
                        acabat = false;
                        if(estat == 5 || (estat < 5 && superior_dreta_valid(pos, col, c_adversari))) {
                                --i;
                                ++j;
                                while (i >= 0 && j <= 7 && !acabat) {
                                        if (this.tauler[i][j].voltejable(col)) {
                                                tauler[i][j].voltejar();
                                                // actualitzem les fitxes del tauler
                                                actualitza_fitxes(col);
                                        } else acabat = true;
                                        --i;
                                        ++j;
                                }
                        }
                        // caturem la diagonal inferior esquerra
                        j = pos.getValue();
                        i = pos.getKey();
                        acabat = false;
                        if(estat == 6 || (estat < 6 && inferior_esquerre_valid(pos, col, c_adversari))) {
                                ++i;
                                --j;
                                while (i <= 7 && j >= 0 && !acabat) {
                                        if (this.tauler[i][j].voltejable(col)) {
                                                tauler[i][j].voltejar();
                                                // actualitzem les fitxes del tauler
                                                actualitza_fitxes(col);
                                        } else acabat = true;
                                        ++i;
                                        --j;
                                }
                        }


                        // captura la diagonal inferior dreta
                        j = pos.getValue();
                        i = pos.getKey();
                        acabat = false;
                        if(estat == 7 || (estat < 7 && inferior_dreta_valid(pos, col, c_adversari))) {
                                ++i;
                                ++j;
                                while (j <= 7 && i <= 7 && !acabat) {
                                        if (this.tauler[i][j].voltejable(col)) {
                                                tauler[i][j].voltejar();
                                                // actualitzem les fitxes del tauler
                                                actualitza_fitxes(col);
                                        } else acabat = true;
                                        ++i;
                                        ++j;
                                }
                        }
                }
        }

        /**
         * Mira si pot col·locar una fitxa de color col en la posició pos.
         * @param pos posició en la que es vol posar la fitxa
         * @param col color de la fitxa que es vol posar
         * @return retorna true si ha posat la fitxa i false en cas contrari
         */
        public boolean posarFitxa (Pair pos , Color col) {
                // Mira si pot col·locar una fitxa de color col en la posició pos. Si es pot, la posa i captura.
                int estat = posicioValida2(pos, col);
                if(estat != -1) {
                        // afegim la fitxa de color col a la posició pos
                        tauler[pos.getKey()][pos.getValue()].color = col;
                        // actualitzem el número de fitxes
                        if (col == Color.Blanc) ++this.fitxesB;
                        else ++this.fitxesN;
                        //capturem totes les fitxes possibles
                        captura(pos, col, estat);
                        return true;
                }
                return false;
        }

        /**
         * Mira si els parametres estan dins del rangs del tauler
         * @param i fila i-èsima del taler
         * @param j columna j-èsima del tauler
         * @return si la posició esta dins del tauler retorna true i en cas contrari retorna false
         */
        private boolean posiciodinstauler(int i, int j) {
                return i >= 0 && j >=0 && i <= 7 && j <= 7;
        }

        /**
         * Mira si es pot possar almenys una fitxa del color negre si negre és true o blanc si negre = false
         * @param negre boolean que indica si el color és negre si negre es true o blanc si negre es false
         * @return retorna true si hi hi ha moviments a fer amb una fitxa negre(si negre = true) o blanca
         * (si negre = false). Retorna false si no hi ha almenys un moviments a fer amb una fitxa de color negre
         * (si negre = true)o blanca (si negre = false).
         */
        public boolean  hihamoviments(boolean negre) {
                Color c_fitxa;
                if(negre) c_fitxa = Color.Negre;
                else c_fitxa = Color.Blanc;
                for(int i = 0; i < 8; ++i) {
                        for (int j = 0; j < 8; ++j) {
                                Pair p1 = new Pair(i,j);
                                if(posicioValida(p1, c_fitxa)) return true;
                                // Si almenys és pot posar una fitxa, hi ha moviments
                        }
                }
                return false; // Si al mirar de posar fitxa en totes les caselles no es pot posar en cap, no hi ha
                // moviments.
        }

        /**
         * @return retorna true si en el tauler no es poden posar cap fitxa per continuar una partida,
         * és a dir, que ja ha acabat. Retorna false si algú pot possar fitxa, és a dir, encara no ha acabat la prtida
         */
        public boolean isacabat() {
                Pair p1;
                if(fitxesB + fitxesN == 64 || fitxesB == 0 || fitxesN == 0) return true;
                // si totes les caselles estan omplertes o no hi ha fitxes d'un color en el tauler, ja ha acabat la partida.
                if(hihamoviments(true)) return false;
                if(hihamoviments(false)) return false;
                else return true;

        }

        /** Durant una partida, mira si una fitxa amb color colr i posició p es pot posar.
         * @param p posició en la que es vl posar la fitxa
         * @param color color de la fitxa a posar
         * @return retorna -1 si una fitxa amb color colr i es pot posar en la posició p.  si no es pot posar
         */
        private int posicioValida2 (Pair p, Color color){
                if(posiciodinstauler(p.getKey(), p.getValue()) && tauler[p.getKey()][p.getValue()].color == Color.Buit) {
                        return capturable(p, color);
                }
                return -1;
        }

        /**
         * Mira si durant una partida es pot afegir una fitxa de color col en la posició pos.
         * @param p posició en la que es vl posar la fitxa
         * @param color color de la fitxa
         * @return retorna true si es pot possar una fitxa de color color en la posició p. En cas contrari retorna false
         */
        public boolean posicioValida(Pair p, Color color){ // mira si durant una partida es pot
                // afegir una fitxa de color col en la posició pos.
                return posiciodinstauler(p.getKey(), p.getValue()) && tauler[p.getKey()][p.getValue()].color == Color.Buit
                        && capturable(p, color) != -1;
        }

        // Lo de sota no s'utilitza per aquesta entrega
        /*+
        /**
         * Mira si hi ha almenys una fitxa adyacent a la fitxa amb posició p
         * @param p posició de la casella a mirar si té alguna altra fitxa adyacent
         * @return retorna true si hi ha almenys 1 fitxa adyacent.
         *         Retorna false si no hi ha cap fitxa adyacent.
         */
        /*
        private boolean fitxesadjacents(Pair p) { // comproba si te almenys 1 fitxa adyacent
                boolean adyacent = false;
                Pair p1;
                for(int i=-1; i< 2 & !(adyacent); ++i){
                        for(int j=-1; j< 2 && !(adyacent); ++j){
                                p1 = new Pair(p.getKey() + i, p.getValue() + j);
                                if(posiciodinstauler(p1.getKey(), p1.getValue())){
                                        if(this.tauler[p.getKey()][p.getValue()].color != Color.Buit && i != 0 && j != 0)
                                                adyacent = true;// comproba si té alguna fitxa adjacent. Si la té,
                                        // ja no cal que miri res més ja que és una posició valida.
                                }
                        }
                }
                return adyacent;
        }
        */
        /*
        /**
         *  Quan s'està creant un tauler, mira si una fitxa amb posició p es posible afegir-la.
         * @param p posició de la fitxa que es vol afegir
         * @return retorna true si la fitxa amb posició p és posible afegir-la. retorna false si no es posible.
         */
        /*
        private boolean posicioValida(Pair p){
                return posiciodinstauler(p.getKey(), p.getValue()) && tauler[p.getKey()][p.getValue()].color == Color.Buit &&  fitxesadjacents(p);
        }

        /**
         * Quan s'està creant el tauler, si es posible afegir la fitxa amb color col i posició p, s'afegeix
         * (actualitzant els atributs de tauler), sinó, no s'afegeix.
         * @param col color de la fitxa a posar
         * @param p posició de la fitxa a posar
         */
        /*
        private void afegir_fitxa(Color col, Pair p) { //afegeix una fitxa de color col en la pos p
                if(posicioValida(p)) {
                        tauler[p.getKey()][p.getValue()].color = col;
                        if (col == Color.Blanc) ++this.fitxesB;
                        else ++this.fitxesN;
                }
        }
        */

}