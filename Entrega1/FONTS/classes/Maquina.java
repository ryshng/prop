package domini.classes;
import domini.controladors.IO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class Maquina extends Jugador {

    /**
     * Constructora de la Màquina
     * @param idJugador id del jugador
     */
    public Maquina(String idJugador) {
        super(idJugador);
    }

    /**
     * Funció per retornar el idJugador Màquina seleccionat
     */
    public Jugador getJugador(){
        return super.getJugador();
    }

    /**
     * Genera tots els moviments possibles (d'una sola jugada) a partir d'un estat inicial i d'un color donat
     * @param tauler Estat del tauler
     * @param color Color de la fitxa que farà els moviments
     * @return Retorna un array amb tots els moviments possibles
     */
    public static ArrayList<Pair> generaMoviments(Tauler tauler, Color color) {
        boolean[][] visitat = new boolean[8][8];
        ArrayList<Pair> moveList = new ArrayList<>();
        LinkedList<Pair> q = new LinkedList<>();
        q.add(new Pair(3,3));
        visitat[3][3] = true;
        Pair p;
        ArrayList<Pair> direccions = new ArrayList<>();
        // Crear un vector de pairs amb les direccions possible amb què es pot capturar una fitxa segons el tauler donat
        if (tauler.getHoritzontal()) direccions.addAll(Arrays.asList(new Pair(1,0), new Pair(-1, 0)));
        if (tauler.getVertical()) direccions.addAll(Arrays.asList(new Pair(0,1), new Pair(0, -1)));
        if (tauler.getDiagonals()) direccions.addAll(Arrays.asList(new Pair(-1,-1), new Pair(1, 1), new Pair(1, -1), new Pair(-1, 1)));
        while (!q.isEmpty()) {
            p = q.pop();
            for (Pair direccio : direccions) {
                int x = p.getKey() + direccio.getKey();
                int y = p.getValue() + direccio.getValue();
                Pair nou = new Pair(x, y);
                if (0 <= x && x <= 7 && 0 <= y && y <= 7 && !visitat[x][y]) {
                    visitat[x][y] = true;
                    if (tauler.posicioValida(nou, color)) {
                        moveList.add(nou);
                    } else if (tauler.getTauler()[x][y].getcolor() != Color.Buit) {
                        q.add(nou);
                    }
                }
            }
        }
        return moveList;
    }



    /**
     * Donat un torn, retorna el torn contrari
     * @param torn Torn actual
     * @return torn contrari
     */
    private static Color canviaTorn(Color torn) {
        if (torn == Color.Buit) { return torn; }
        if (torn == Color.Blanc) { return Color.Negre; }
        return Color.Blanc;
    }

    /**
     * Funció que calcula el proper moviment que farà la màquina, aplicant l'algoritme MiniMax. Es considera que el jugador que té el torn actual és el maximitzant, i el contrincant és el minimitzant
     * @param tauler Estat actual del joc
     * @param torn Color de la fitxa que farà la jugada
     * @return Posició on es colocarà la fitxa en la següent jugada
     */
    public static Pair decidirJugada(Tauler tauler, Color torn, int prof, boolean podes, boolean heuristic) throws CloneNotSupportedException {
        ArrayList<Pair> moveList = generaMoviments(tauler, torn);
        Pair millorMoviment = null;
        int maxim = -10000; // TODO: Mirar si es poden utilitzar infinits
        int maximActual;
        for(Pair move : moveList) {
            // S'aplica el moviment al tauler;
            Tauler t = tauler.clone();
            if (t.posarFitxa(move, torn)) {
                // Es calcula el valor de la jugada
                if (podes) {
                    // Exploració de l'arbre aplicant-hi podes alfa beta
                    maximActual = valorMiniMaxPodes(t, -10000, 10000, prof-1, canviaTorn(torn), false, heuristic);
                }
                else {
                    // Exploració de l'arbre sense aplicar-hi podes.
                    maximActual = valorMiniMax(t, prof-1, canviaTorn(torn), false, heuristic);
                }
                // Si la jugada és millor que totes les anteriors, s'actualitza el millor moviment
                if (maximActual > maxim) {
                    maxim = maximActual;
                    millorMoviment = move;
                }
            }
        }
        return millorMoviment;
    }

    /**
     * Retorna el valor heurístic d'un estat de joc (sense podes)
     * @param tauler estat de joc
     * @param prof Nivells de profunditat
     * @param torn Color de la casella
     * @return valor heurístic de l'estat del joc
     */
    private static int valorMiniMax(Tauler tauler, int prof, Color torn, boolean maximitzant, boolean heuristic) throws CloneNotSupportedException {
        int millorValor;
        if (prof == 0 || tauler.isacabat()) {
            if (heuristic) return Heuristica.heuristic2(tauler, torn);
            else return Heuristica.heuristic1(tauler, torn);
        } else {
            // Es generen tots els moviments possibles en un tauler i per un color de fitxa
            ArrayList<Pair> moveList = generaMoviments(tauler, torn);
            if (maximitzant) {
                // Node maximitzant
                millorValor = -10000;
                for (Pair move : moveList) {
                    Tauler t = tauler.clone();
                    if (t.posarFitxa(move, torn)) {
                        millorValor = Math.max(millorValor, valorMiniMax(tauler, prof-1, canviaTorn(torn), false, heuristic));
                    }
                }
                return millorValor;
            }
            else {
                // Node minimitzant
                millorValor = 10000;
                for (Pair move : moveList) {
                    Tauler t = tauler.clone();
                    if (t.posarFitxa(move, torn)) {
                        millorValor = Math.min(millorValor, valorMiniMax(tauler, prof-1, canviaTorn(torn), true, heuristic));
                    }
                }
                return millorValor;
            }
        }
    }

    /**
     * Retorna el valor heurístic d'un estat de joc aplicant-hi podes
     * @param tauler estat de joc
     * @param alfa Cota inferior del valor que es pot assignar a un node maximitzant
     * @param beta Cota superior del valor que es pot assignar a un node maximitzant
     * @param prof Nivells de profunditat
     * @param torn Color de la casella
     * @return valor heurístic de l'estat del joc
     */
    private static int valorMiniMaxPodes(Tauler tauler, int alfa, int beta, int prof, Color torn, boolean maximitzant, boolean heuristic) throws CloneNotSupportedException {
        if (prof == 0 || tauler.isacabat()) {
            if (heuristic) return Heuristica.heuristic2(tauler, torn);
            else return Heuristica.heuristic1(tauler, torn);
        } else {
            // Es generen tots els moviments possibles en un tauler i per un color de fitxa
            ArrayList<Pair> moveList = generaMoviments(tauler, torn);
            // Per cada un dels moviments es calcula el valor heurístic del tauler resultant
            if (maximitzant) {
                for (Pair move : moveList) {
                    Tauler t = tauler.clone();
                    if (t.posarFitxa(move, torn)) {
                        alfa = Math.max(alfa, valorMiniMaxPodes(t, alfa, beta, prof-1, canviaTorn(torn), false, heuristic));
                        if (alfa >= beta) return beta;
                    }
                }
                return alfa;
            }
            else {
                for (Pair move : moveList) {
                    Tauler t = tauler.clone();
                    if (t.posarFitxa(move, torn)) {
                        beta = Math.min(beta, valorMiniMaxPodes(t, alfa, beta, prof-1, canviaTorn(torn), true, heuristic));
                        if (beta <= alfa) return alfa;
                    }
                }
                return beta;
            }
        }
    }
}
