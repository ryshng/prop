package domini.shared;
/*
 * Classe Posicio
 */
public class Pair {
    private int x;
    private int y;

    /*
     * Getters i Setters
     */
    public int getKey() { return x; }

    public int getValue() {
        return y;
    }

    public void setKey(int x) {
        this.x = x;
    }

    public void setValue(int y) {
        this.y = y;
    }

    /*
     * Constructores
     */
    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }
}

